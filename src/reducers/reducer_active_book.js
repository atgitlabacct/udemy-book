// State argument is not application state, but only the state
// that the reducer is responsible for.
export default function(state = null, action) {
  switch(action.type) {
    case 'BOOK_SELECTED':
      // Don't chance the state...return a fresh object
      return action.payload;
  }

  return state;
}
