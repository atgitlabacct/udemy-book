export default function() {
  return [
    { title: 'Harry Potter', pages: 101 },
    { title: 'Javscript the Good Parts', pages: 39 },
    { title: 'Dark Tower', pages: 85 },
    { title: 'Eloquent Ruby', pages: 1 }
  ];
}
