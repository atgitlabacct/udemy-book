import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

export class BookDetail extends Component {
  render() {
    if (this.props.book === null) {
      return (
        <div>Select a book</div>
      )
    }

    return (
      <div>
        <h3>Details for:</h3>
        <div>Title: {this.props.book.title}</div>
        <div>Pages: {this.props.book.pages}</div>
      </div>
    )
  };
}

function mapStateToProps(state) {
  return {
    book: state.activeBook
  };
}

// Promote BookList from a component to a container - it needs to know about
// this new dispatch method, selectBook.  Make it available as a prop.
export default connect(mapStateToProps)(BookDetail)
