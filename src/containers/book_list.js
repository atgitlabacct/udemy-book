import React, { Component } from 'react';
import { selectBook } from '../actions/index';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class BookList extends Component {
  renderList() {
    return this.props.books.map((book) => {
      return (
        <li
          key={book.title}
          className="list-group-item"
          onClick={() => this.props.selectBook(book) }>
          {book.title}
        </li>
      )
    });
  }

  render() {
    return (
      <ul className="list-group col-sm-4">
        { this.renderList() }
      </ul>
    );
  }
}


// state here referes to the Application State not component state
// This will also end up on the BookList container props.
function mapApplicationStateToProps(state) {
  return {
    books: state.books
  };
}

// Anything returned from this function will end up as props
// on the BookList Container
function mapDispatchToProps(dispatch) {
  // Whenever select book is called results should be passed
  // to all of our reducers
  return bindActionCreators({ selectBook: selectBook }, dispatch)
}

// Promote BookList from a component to a container - it needs to know about
// this new dispatch method, selectBook.  Make it available as a prop.
export default connect(mapApplicationStateToProps, mapDispatchToProps)(BookList)
