// ActionCreator's are simply functions that return an Action.  The Action
// is a object with a type and any other attributes you might assign.
export function selectBook(book) {
    // selectBook is an ActionCreator, it needs to return an action,
    // an object with a type property.
    return {
        type: 'BOOK_SELECTED',
        payload: book
    };
}
